with Ada.Text_IO;

use Ada.Text_IO;

procedure Nth_Prime is
   Place     : Integer := 6;
   Nth_Prime : Integer := 0;
   Primes    : array (Integer 2 .. 1_000_000) := (2 .. 1_000_000);
   
   procedure Prime_Seive (Max : Integer) is
      N : Integer := 2;
   begin
      while N * N <= Max loop
         
         if Primes (N) then
            Put_Line (Long_Natural'Image (N) & " is a prime number.");
            
            REMOVE_MULTIPLES:
            declare
               Multiple : Long_Natural := N * N; -- starting at the square is a common optimization
               Coefficient : Long_Natural := 1;
            begin
               while Multiple <= Primes'Last loop
                  Primes (Multiple) := false;
                  Put_Line ("Removing multiples of" &
                              Long_Natural'Image (N) &
                                ":" & Long_Natural'Image (Multiple));
                  Multiple := N * N + Coefficient * N;
                  Coefficient := Coefficient + 1;
               end loop;
            end REMOVE_MULTIPLES;
         end if;
         
         N := N + 1;
      end loop;
   end SIEVE;
   
begin
   
end Nth_Prime;