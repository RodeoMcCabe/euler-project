with Ada.Text_IO;
use Ada.Text_IO;

procedure Smallest_Multiple is
   type Factor_Range is new Long_Integer range 2 .. 20;
    
   Smallest   : Long_Integer := 0;
   Test_Value : Long_Integer := 0;
   
   
   function Is_Divisible return Boolean is
      Divisible_By_All_Factors : Boolean := true;
   begin
      for Factor in Factor_Range'Range loop
         if Test_Value mod Long_Integer (Factor) /= 0 then
            Put_Line (Long_Integer'Image (Test_Value) &
                        " is not divisible by" &
                           Factor_Range'Image (Factor));
            Divisible_By_All_Factors := false;
            exit;
         end if;
      end loop;
      return Divisible_By_All_Factors;
   end Is_Divisible;


begin
   
   INITIALIZE:
   begin
   -- Start from the largest factor.
      Test_Value := Long_Integer (Factor_Range'Last);

--      for Factor in Factor_Range'First + 1 .. Factor_Range'Last loop
--         Test_Value := Test_Value * Long_Integer (Factor);
--      end loop;

      Smallest := Test_Value;
   end INITIALIZE;
      
   MAIN_LOOP:
   loop
      if Is_Divisible then
         Smallest := Test_Value;
         Put_Line ("Smallest value found!" &
                     Long_Integer'Image (Smallest) &
                       " is divisible by all the factors!");
         exit MAIN_LOOP;
      else
         -- increment by the largest factor
         Test_Value := Test_Value + Long_Integer (Factor_Range'Last);
      end if;
   end loop MAIN_LOOP;

   Put_Line ("The smallest number that is evenly divisibly by all the numbers" &
               " from 1 to 20 is" &
                 Long_Integer'Image (Smallest));

end Smallest_Multiple;