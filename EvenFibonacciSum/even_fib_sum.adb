with Ada.Text_IO;
use Ada.Text_IO;

procedure Even_Fib_Sum is
   Total    : Natural  := 0;
   Next     : Natural  := 0;
   Current  : Positive := 1;
   Previous : Positive := 1; 
begin
   while Current < 4_000_000 loop
      if Current mod 2 = 0 then
         Total := Total + Current;
         Put_Line ("Total:" & Integer'Image (Total));
      end if;

      Next := Current + Previous;
      Previous := Current;
      Current := Next;
      
      -- Put_Line ("Previous:" & Integer'Image (Previous));
      Put_Line ("Current:" & Integer'Image (Current));
      -- Put_Line ("Next:" & Integer'Image (Next));      
   end loop;
end Even_Fib_Sum;