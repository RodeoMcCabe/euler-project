with Ada.Text_IO;
use Ada.Text_IO;

procedure Prime_Factors is

   type Long_Natural is range 0 .. Long_Integer'Last;
   type Prime_Array is array (Long_Natural range <>) of Boolean;

   Number        : Long_Natural := 5_475_143;
   Primes        : Prime_Array (2 .. Number) := (others => true);
   Largest_Prime : Long_Natural := 0;
   
begin
   Put_Line ("Finding primes below" & Long_Natural'Image (Number / 2) & " ....");

   SIEVE:
   declare
      N : Long_Natural := 2;
   begin
      while N * N <= Number loop
         
         if Primes (N) then
            Put_Line (Long_Natural'Image (N) & " is a prime number.");
            
            REMOVE_MULTIPLES:
            declare
               Multiple : Long_Natural := N * N; -- starting at the square is a common optimization
               Coefficient : Long_Natural := 1;
            begin
               while Multiple <= Primes'Last loop
                  Primes (Multiple) := false;
                  Put_Line ("Removing multiples of" &
                              Long_Natural'Image (N) &
                                ":" & Long_Natural'Image (Multiple));
                  Multiple := N * N + Coefficient * N;
                  Coefficient := Coefficient + 1;
               end loop;
            end REMOVE_MULTIPLES;
         end if;
         
         N := N + 1;
      end loop;
   end SIEVE;

   Put_Line ("Finding prime factors of" & Long_Natural'Image (Number) & " ....");

   FACTORIZE: -- keep dividing by the smallest prime number possible
   declare
      Quotient : Long_Natural := Number;
   begin
      while not Primes (Quotient) loop
            
         for N in Primes'Range loop
            if Primes (N) then
               if Quotient mod N = 0 then
               
                  Quotient := Quotient / N;
                  Put_Line ("Quotient is" & Long_Natural'Image (Quotient));
               
                  if N > Largest_Prime then
                     Put_Line (Long_Natural'Image (N) & " is a PRIME FACTOR!");
                     Largest_Prime := N;
                  end if;
                  exit;
                  
               end if;
            end if;
         end loop;
      end loop;
      
      Put_Line (Long_Natural'Image (Quotient) & " is a PRIME FACTOR!");
      Largest_Prime := Quotient;
   end FACTORIZE;
      
   Put_Line ("The largest prime factor is" &
             Long_Natural'Image (Largest_Prime));
end Prime_Factors;