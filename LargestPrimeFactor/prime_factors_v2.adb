with Ada.Text_IO;
use Ada.Text_IO;

procedure Prime_Factors is

   type Long_Natural is range 0 .. Long_Integer'Last;

   Number        : Long_Natural := 600851475143;
   Divisor       : Long_Natural := 2;
   Largest_Prime : Long_Natural := 0;
   Found         : Boolean      := false;
   
   
   function Is_Prime (Int : Long_Natural) return Boolean is
   begin
      for N in 2 .. Int / 2 loop
         if Number mod N = 0 then
            Put_Line (Long_Natural'Image (Int) & " is not prime.");
            return false;
         end if;
      end loop;
      Put_Line (Long_Natural'Image (Int) & " is a PRIME number!");
      return true;
   end Is_Prime;
   
begin
   Put_Line ("Finding factors of" & Long_Natural'Image (Number) & " ....");
   loop
      exit when Found or Divisor > Number / 2;
         
      if Number mod Divisor = 0 then
         Put_Line ("Factors found:" & 
                   Long_Natural'Image (Divisor) &
                   Long_Natural'Image (Number / Divisor));
         if Is_Prime (Number / Divisor) then
            Largest_Prime := Number / Divisor;
            Found := true;
         end if;
      end if;
      
      Divisor := Divisor + 1;
   end loop;
   
   New_Line;
   Put_Line ("The largest prime factor is" &
             Long_Natural'Image (Largest_Prime));
end Prime_Factors;