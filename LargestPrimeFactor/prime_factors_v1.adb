with Ada.Text_IO,
     Ada.Numerics,
     Ada.Containers.Ordered_Sets;
use Ada.Text_IO,
    Ada.Containers;

procedure Prime_Factors is

   type Long_Natural is range 0 .. Long_Integer'Last;

   package Factor_Set is new Ordered_Sets (Long_Natural);
   use Factor_Set;

   Number        : Long_Natural := 55_555_423;
   Largest_Prime : Long_Natural := 0;
   Factors       : Set;
   
   function Get_Factors (Int : Long_Natural) return Set is
      To_Check : Set;     
      Divisor  : Long_Natural;
      Factors  : Set;
   begin
      Put_Line ("Finding factors for" & Long_Natural'Image (Int) & " ....");
      -- initialize numbers to check
      for J in 2 .. Int / 2 loop
         Insert (To_Check, J);
      end loop;
      
      while not Is_Empty (To_Check) loop
         Divisor := First_Element (To_Check);
         if Int mod Divisor = 0 then
            Put_Line ("Factors found!" &
                      Long_Natural'Image (Divisor) &
                      "," &
                      Long_Natural'Image (Int / Divisor));
            Insert (Factors, Divisor);
            Delete (To_Check, Divisor);
            
            if Divisor /= Int / Divisor then
               Insert (Factors, Int / Divisor);
               Delete (To_Check, Int / Divisor);
            end if;
         else
            Delete (To_Check, First (To_Check));
         end if;
      end loop;      
      if Is_Empty (Factors) then
         Put_Line ("No factors found!" &
                   Long_Natural'Image (Int) &
                   " is PRIME!");
      end if; 
      return Factors;
   end Get_Factors;
   
begin
   Factors  := Get_Factors(Number);
   
   for F of Factors loop
      New_Line;
      Put_Line ("Checking if" & Long_Natural'Image (F) & " is prime ....");
      --if Is_Empty (Get_Factors (Element (Position))) then -- must be prime
      if Ada.Numerics.Is_Prime (F) then
         New_Line;
         Put_Line ("PRIME FACTOR found:" &
                   Long_Natural'Image (F));
         New_Line;
         Largest_Prime := F;
      else
         Put_Line ("Factor" & Long_Natural'Image (F) & " is not prime.");
      end if;
   end loop;
   
   New_Line;
   Put_Line ("The largest prime factor is" &
             Long_Natural'Image (Largest_Prime) &
             "!");
end Prime_Factors;