with Ada.Text_IO,
     Ada.Integer_Text_IO;


procedure Multiple_Sum is
   package IO renames Ada.Text_IO;
   Total : Natural := 0;
begin
   for Int in 1 .. 999 loop
      if Int mod 3 = 0 then
         IO.Put_Line ("Divisible by 3:" & Integer'Image (Int));
         Total := Total + Int;
      elsif Int mod 5 = 0 then
         Total := Total + Int;
         IO.Put_Line ("Divisible by 5:" & Integer'Image (Int));
      end if;
   end loop;
   
   IO.Put_Line ("Total:" & Integer'Image (Total));
end Multiple_Sum;