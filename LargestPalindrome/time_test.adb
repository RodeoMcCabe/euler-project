with Ada.Text_IO,
     Ada.Strings.Fixed;

use Ada.Text_IO,
    Ada.Strings;

procedure Time_Test is

   Is_Palindrome : Boolean := false;
   Largest       : Natural := 0;
   
   function String_Palindrome (Number : String) return Boolean is
      Backward : String (Number'Range);
      Result   : Boolean := false;
   begin
      -- if there's only 1 digit, it's a palindrome
      if Number'Last = 1 then
         return true;
      end if;
      
      for Letter in Number'Range loop
         Backward ((Number'Last + 1) - Letter) := Number (Letter);
      end loop;
      
      if Number = Backward then
         Result := true;
      end if;
      return Result;
   end String_Palindrome;
   
   
   function Modulo_Palindrome (Number : Natural) return Boolean is
      Forward   : Natural := Number;
      Backward  : Natural := 0;
      Remainder : Natural := 0;
      Result    : Boolean := false;
   begin
      if Number < 10 then
         return true;
      end if;
      
      while Forward >= 10 loop
         Remainder := Forward mod 10;
         Backward := Backward * 10 + Remainder;
         Forward := Forward / 10;
      end loop;
      
      -- add the last digit left over in Forward
      Backward := Backward * 10 + Forward;      
            
      if Backward = Number then
         Result := true;
      end if;
      return Result;
   end Modulo_Palindrome;


begin
   INPUT_LOOP:
   loop
      Put_Line ("Enter a number to check if it's a palindrome.");
      
      READ_INPUT:
      declare
         Input  : String  := Get_Line;
         Number : Natural := 0;
      begin
         Number := Natural'Value (Input);
         Put_Line ("Performing string reverse method ...");
         Is_Palindrome := String_Palindrome (Input);
         
         if Is_Palindrome then
            Put_Line ("That's a PALINDROME!");
         else
            Put_Line ("Just another boring old number.");
            New_Line;
         end if;
         
         Put_Line ("Performing modulo operator method ...");
         Is_Palindrome := Modulo_Palindrome (Number);
         
         if Is_Palindrome then
            Put_Line ("That's a PALINDROME!");
         else
            Put_Line ("Just another boring old number.");
            New_Line;
         end if;
         
         exit INPUT_LOOP;
      exception
         when CONSTRAINT_ERROR =>
            New_Line;
            Put_Line ("Invalid input! Must be 0 to" &
                        Natural'Image (Natural'Last));
            New_Line;
      end READ_INPUT;
   end loop INPUT_LOOP;
   
end Time_Test;
