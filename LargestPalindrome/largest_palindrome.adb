with Ada.Text_IO,
     Ada.Strings.Fixed;

use Ada.Text_IO,
    Ada.Strings;

procedure Largest_Palindrome is

   Is_Palindrome : Boolean := false;
   Largest       : Natural := 0;
   
   function String_Palindrome (Number : String) return Boolean is
      Backward : String (Number'Range);
      Result   : Boolean := false;
   begin
      -- if there's only 1 digit, it's a palindrome
      if Number'Last = 1 then
         return true;
      end if;
      
      for Letter in Number'Range loop
         Backward ((Number'Last + 1) - Letter) := Number (Letter);
      end loop;
      
      if Number = Backward then
         Result := true;
      end if;
      return Result;
   end String_Palindrome;
   
   
   function Modulo_Palindrome (Number : Natural) return Boolean is
      Forward   : Natural := Number;
      Backward  : Natural := 0;
      Remainder : Natural := 0;
      Result    : Boolean := false;
   begin
      if Number < 10 then
         return true;
      end if;
      
      while Forward >= 10 loop
         Remainder := Forward mod 10;
         Backward := Backward * 10 + Remainder;
         Forward := Forward / 10;
      end loop;
      
      -- add the last digit left over in Forward
      Backward := Backward * 10 + Forward;      
            
      if Backward = Number then
         Result := true;
      end if;
      return Result;
   end Modulo_Palindrome;


begin
   Put_Line ("Finding the largest palindrome product of two 3-digit numbers ...");

   LH_LOOP:
   for J in reverse 1 .. 999 loop
      RH_LOOP:
      for K in reverse 1 .. 999 loop
         CHECK_PALINDROME:
         declare
            Product : Natural := J * K;
            Is_Palindrome : Boolean := Modulo_Palindrome (Product);
         begin
            if Is_Palindrome then
               Put_Line ("PALINDROME!" &
                           Natural'Image (J) & " x" &
                             Natural'Image (K) & " =" &
                               Natural'Image (Product));
               if Product > Largest then
                  Largest := Product;
               end if;
            end if;
         end CHECK_PALINDROME;
      end loop RH_LOOP;
      Put_Line ("Decrementing LH operand to" & Natural'Image (J - 1));
   end loop LH_LOOP;
   
   New_Line;
   Put_Line ("The largest palindromic product of two 3-digit numbers is" &
               Natural'Image (Largest));
end Largest_Palindrome;