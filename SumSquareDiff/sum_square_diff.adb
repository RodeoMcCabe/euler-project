with Ada.Text_IO;
use  Ada.Text_IO;


procedure Sum_Square_Diff is
   
   type Domain is new Integer range 1 .. 100;
   
   Sum_Of_Squares : Integer := 0;
   Square_Of_Sum  : Integer := 0;
   Difference     : Integer := 0;
   
begin
   SUM_SQUARES:
   begin
      for Number in Domain'Range loop
         Sum_Of_Squares := Sum_Of_Squares + Integer (Number * Number);
         Put_Line ("Sum of squares is" & Integer'Image (Sum_Of_Squares));
      end loop;
   end SUM_SQUARES;
   
   SQUARE_SUM:
   begin
      for Number in Domain'Range loop
         Square_Of_Sum := Square_Of_Sum + Integer (Number);
         Put_Line ("Sum is" & Integer'Image (Square_Of_Sum));
      end loop;
      Square_Of_Sum := Square_Of_Sum * Square_Of_Sum;
      Put_Line ("Square of sum is" & Integer'Image (Square_Of_Sum));
   end SQUARE_SUM;
   
   Difference := Square_Of_Sum - Sum_Of_Squares;
   Put_Line ("Difference is" & 
               Integer'Image (Square_Of_Sum) & " -" &
               Integer'Image (Sum_Of_Squares) & " =" &
               Integer'Image (Difference));
end Sum_Square_Diff;